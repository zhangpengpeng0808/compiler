/* A Bison parser, made by GNU Bison 3.7.6.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_HOME_ZIPPERMONKEY_SYC_BUILD_PARSER_HPP_INCLUDED
# define YY_YY_HOME_ZIPPERMONKEY_SYC_BUILD_PARSER_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    INTEGER_VALUE = 258,           /* INTEGER_VALUE  */
    IDENTIFIER = 259,              /* IDENTIFIER  */
    IF = 260,                      /* IF  */
    ELSE = 261,                    /* ELSE  */
    WHILE = 262,                   /* WHILE  */
    FOR = 263,                     /* FOR  */
    BREAK = 264,                   /* BREAK  */
    CONTINUE = 265,                /* CONTINUE  */
    RETURN = 266,                  /* RETURN  */
    CONST = 267,                   /* CONST  */
    INT = 268,                     /* INT  */
    VOID = 269,                    /* VOID  */
    ASSIGN = 270,                  /* ASSIGN  */
    EQ = 271,                      /* EQ  */
    NE = 272,                      /* NE  */
    LT = 273,                      /* LT  */
    LE = 274,                      /* LE  */
    GT = 275,                      /* GT  */
    GE = 276,                      /* GE  */
    AND = 277,                     /* AND  */
    OR = 278,                      /* OR  */
    LPAREN = 279,                  /* LPAREN  */
    RPAREN = 280,                  /* RPAREN  */
    LBRACE = 281,                  /* LBRACE  */
    RBRACE = 282,                  /* RBRACE  */
    LSQUARE = 283,                 /* LSQUARE  */
    RSQUARE = 284,                 /* RSQUARE  */
    DOT = 285,                     /* DOT  */
    COMMA = 286,                   /* COMMA  */
    SEMI = 287,                    /* SEMI  */
    PLUSPLUS = 288,                /* PLUSPLUS  */
    MINUSMINUS = 289,              /* MINUSMINUS  */
    PLUS = 290,                    /* PLUS  */
    MINUS = 291,                   /* MINUS  */
    MUL = 292,                     /* MUL  */
    DIV = 293,                     /* DIV  */
    MOD = 294,                     /* MOD  */
    NOT = 295                      /* NOT  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 32 "src/parser.y"

    int token;
    NIdentifier* ident;
    NExpression* expr;
    NRoot* root;
    NDeclareStatement* declare_statement;
    NFunctionDefine* fundef;
    NDeclare* declare;
    NArrayDeclareInitValue* array_init_value;
    NArrayIdentifier* array_identifier;
    NFunctionCallArgList* arg_list;
    NFunctionDefineArgList* fundefarglist;
    NFunctionDefineArg* fundefarg;
    NBlock* block;
    NStatement* stmt;
    NAssignment* assignmentstmt;
    NIfElseStatement* ifelsestmt;
    NConditionExpression* condexp;
    std::string *string;

#line 125 "/home/zippermonkey/syc/build/parser.hpp"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_HOME_ZIPPERMONKEY_SYC_BUILD_PARSER_HPP_INCLUDED  */
