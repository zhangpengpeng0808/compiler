/* A Bison parser, made by GNU Bison 3.7.6.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30706

/* Bison version string.  */
#define YYBISON_VERSION "3.7.6"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 18 "src/parser.y"

#include "node.h"
#include <cstdio>
#include <cstdlib>
NRoot *root; /* the top level root node of our final AST */
extern int yydebug;

extern int yylex();
extern int yyget_lineno();
void yyerror(const char *s) { std::printf("Error(line: %d): %s\n", yyget_lineno(), s); if (!yydebug) std::exit(1); }
#define YYERROR_VERBOSE true
#define YYDEBUG 1

#line 85 "/home/zippermonkey/syc/build/parser.cpp"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "parser.hpp"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_INTEGER_VALUE = 3,              /* INTEGER_VALUE  */
  YYSYMBOL_IDENTIFIER = 4,                 /* IDENTIFIER  */
  YYSYMBOL_IF = 5,                         /* IF  */
  YYSYMBOL_ELSE = 6,                       /* ELSE  */
  YYSYMBOL_WHILE = 7,                      /* WHILE  */
  YYSYMBOL_FOR = 8,                        /* FOR  */
  YYSYMBOL_BREAK = 9,                      /* BREAK  */
  YYSYMBOL_CONTINUE = 10,                  /* CONTINUE  */
  YYSYMBOL_RETURN = 11,                    /* RETURN  */
  YYSYMBOL_CONST = 12,                     /* CONST  */
  YYSYMBOL_INT = 13,                       /* INT  */
  YYSYMBOL_VOID = 14,                      /* VOID  */
  YYSYMBOL_ASSIGN = 15,                    /* ASSIGN  */
  YYSYMBOL_EQ = 16,                        /* EQ  */
  YYSYMBOL_NE = 17,                        /* NE  */
  YYSYMBOL_LT = 18,                        /* LT  */
  YYSYMBOL_LE = 19,                        /* LE  */
  YYSYMBOL_GT = 20,                        /* GT  */
  YYSYMBOL_GE = 21,                        /* GE  */
  YYSYMBOL_AND = 22,                       /* AND  */
  YYSYMBOL_OR = 23,                        /* OR  */
  YYSYMBOL_LPAREN = 24,                    /* LPAREN  */
  YYSYMBOL_RPAREN = 25,                    /* RPAREN  */
  YYSYMBOL_LBRACE = 26,                    /* LBRACE  */
  YYSYMBOL_RBRACE = 27,                    /* RBRACE  */
  YYSYMBOL_LSQUARE = 28,                   /* LSQUARE  */
  YYSYMBOL_RSQUARE = 29,                   /* RSQUARE  */
  YYSYMBOL_DOT = 30,                       /* DOT  */
  YYSYMBOL_COMMA = 31,                     /* COMMA  */
  YYSYMBOL_SEMI = 32,                      /* SEMI  */
  YYSYMBOL_PLUSPLUS = 33,                  /* PLUSPLUS  */
  YYSYMBOL_MINUSMINUS = 34,                /* MINUSMINUS  */
  YYSYMBOL_PLUS = 35,                      /* PLUS  */
  YYSYMBOL_MINUS = 36,                     /* MINUS  */
  YYSYMBOL_MUL = 37,                       /* MUL  */
  YYSYMBOL_DIV = 38,                       /* DIV  */
  YYSYMBOL_MOD = 39,                       /* MOD  */
  YYSYMBOL_NOT = 40,                       /* NOT  */
  YYSYMBOL_YYACCEPT = 41,                  /* $accept  */
  YYSYMBOL_CompUnit = 42,                  /* CompUnit  */
  YYSYMBOL_Decl = 43,                      /* Decl  */
  YYSYMBOL_BType = 44,                     /* BType  */
  YYSYMBOL_ConstDeclStmt = 45,             /* ConstDeclStmt  */
  YYSYMBOL_ConstDecl = 46,                 /* ConstDecl  */
  YYSYMBOL_VarDeclStmt = 47,               /* VarDeclStmt  */
  YYSYMBOL_VarDecl = 48,                   /* VarDecl  */
  YYSYMBOL_Def = 49,                       /* Def  */
  YYSYMBOL_DefOne = 50,                    /* DefOne  */
  YYSYMBOL_DefArray = 51,                  /* DefArray  */
  YYSYMBOL_ConstDef = 52,                  /* ConstDef  */
  YYSYMBOL_ConstDefOne = 53,               /* ConstDefOne  */
  YYSYMBOL_ConstDefArray = 54,             /* ConstDefArray  */
  YYSYMBOL_DefArrayName = 55,              /* DefArrayName  */
  YYSYMBOL_InitVal = 56,                   /* InitVal  */
  YYSYMBOL_InitValArray = 57,              /* InitValArray  */
  YYSYMBOL_InitValArrayInner = 58,         /* InitValArrayInner  */
  YYSYMBOL_Exp = 59,                       /* Exp  */
  YYSYMBOL_CommaExpr = 60,                 /* CommaExpr  */
  YYSYMBOL_LOrExp = 61,                    /* LOrExp  */
  YYSYMBOL_LAndExp = 62,                   /* LAndExp  */
  YYSYMBOL_EqExp = 63,                     /* EqExp  */
  YYSYMBOL_RelExp = 64,                    /* RelExp  */
  YYSYMBOL_AddExp = 65,                    /* AddExp  */
  YYSYMBOL_MulExp = 66,                    /* MulExp  */
  YYSYMBOL_UnaryExp = 67,                  /* UnaryExp  */
  YYSYMBOL_FunctionCall = 68,              /* FunctionCall  */
  YYSYMBOL_PrimaryExp = 69,                /* PrimaryExp  */
  YYSYMBOL_ArrayItem = 70,                 /* ArrayItem  */
  YYSYMBOL_LVal = 71,                      /* LVal  */
  YYSYMBOL_FuncDef = 72,                   /* FuncDef  */
  YYSYMBOL_FuncFParams = 73,               /* FuncFParams  */
  YYSYMBOL_FuncFParam = 74,                /* FuncFParam  */
  YYSYMBOL_FuncRParams = 75,               /* FuncRParams  */
  YYSYMBOL_FuncFParamOne = 76,             /* FuncFParamOne  */
  YYSYMBOL_FuncFParamArray = 77,           /* FuncFParamArray  */
  YYSYMBOL_Block = 78,                     /* Block  */
  YYSYMBOL_BlockItems = 79,                /* BlockItems  */
  YYSYMBOL_BlockItem = 80,                 /* BlockItem  */
  YYSYMBOL_Stmt = 81,                      /* Stmt  */
  YYSYMBOL_AssignStmt = 82,                /* AssignStmt  */
  YYSYMBOL_AssignStmtWithoutSemi = 83,     /* AssignStmtWithoutSemi  */
  YYSYMBOL_IfStmt = 84,                    /* IfStmt  */
  YYSYMBOL_ReturnStmt = 85,                /* ReturnStmt  */
  YYSYMBOL_WhileStmt = 86,                 /* WhileStmt  */
  YYSYMBOL_ForStmt = 87,                   /* ForStmt  */
  YYSYMBOL_BreakStmt = 88,                 /* BreakStmt  */
  YYSYMBOL_ContinueStmt = 89,              /* ContinueStmt  */
  YYSYMBOL_Cond = 90,                      /* Cond  */
  YYSYMBOL_Number = 91,                    /* Number  */
  YYSYMBOL_AddOp = 92,                     /* AddOp  */
  YYSYMBOL_MulOp = 93,                     /* MulOp  */
  YYSYMBOL_UnaryOp = 94,                   /* UnaryOp  */
  YYSYMBOL_RelOp = 95,                     /* RelOp  */
  YYSYMBOL_ident = 96                      /* ident  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  15
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   351

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  41
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  56
/* YYNRULES -- Number of rules.  */
#define YYNRULES  125
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  216

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   295


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,    83,    83,    84,    85,    86,    89,    90,    93,    95,
      97,    98,   101,   103,   104,   107,   108,   111,   112,   115,
     116,   119,   120,   123,   126,   129,   130,   133,   135,   136,
     139,   140,   141,   142,   143,   152,   153,   156,   162,   168,
     169,   170,   173,   174,   177,   178,   179,   182,   183,   186,
     187,   190,   191,   194,   195,   196,   199,   200,   203,   204,
     205,   206,   209,   210,   213,   214,   217,   218,   219,   220,
     224,   225,   228,   229,   232,   233,   236,   238,   245,   248,
     249,   252,   253,   256,   257,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   272,   274,   275,   276,   277,
     278,   281,   282,   285,   286,   289,   291,   300,   309,   318,
     320,   322,   324,   326,   327,   330,   331,   332,   335,   336,
     337,   340,   341,   342,   343,   346
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "INTEGER_VALUE",
  "IDENTIFIER", "IF", "ELSE", "WHILE", "FOR", "BREAK", "CONTINUE",
  "RETURN", "CONST", "INT", "VOID", "ASSIGN", "EQ", "NE", "LT", "LE", "GT",
  "GE", "AND", "OR", "LPAREN", "RPAREN", "LBRACE", "RBRACE", "LSQUARE",
  "RSQUARE", "DOT", "COMMA", "SEMI", "PLUSPLUS", "MINUSMINUS", "PLUS",
  "MINUS", "MUL", "DIV", "MOD", "NOT", "$accept", "CompUnit", "Decl",
  "BType", "ConstDeclStmt", "ConstDecl", "VarDeclStmt", "VarDecl", "Def",
  "DefOne", "DefArray", "ConstDef", "ConstDefOne", "ConstDefArray",
  "DefArrayName", "InitVal", "InitValArray", "InitValArrayInner", "Exp",
  "CommaExpr", "LOrExp", "LAndExp", "EqExp", "RelExp", "AddExp", "MulExp",
  "UnaryExp", "FunctionCall", "PrimaryExp", "ArrayItem", "LVal", "FuncDef",
  "FuncFParams", "FuncFParam", "FuncRParams", "FuncFParamOne",
  "FuncFParamArray", "Block", "BlockItems", "BlockItem", "Stmt",
  "AssignStmt", "AssignStmtWithoutSemi", "IfStmt", "ReturnStmt",
  "WhileStmt", "ForStmt", "BreakStmt", "ContinueStmt", "Cond", "Number",
  "AddOp", "MulOp", "UnaryOp", "RelOp", "ident", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295
};
#endif

#define YYPACT_NINF (-153)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     114,    36,  -153,    28,    25,  -153,    28,  -153,    45,  -153,
      54,  -153,    28,  -153,    47,  -153,  -153,  -153,  -153,  -153,
    -153,    12,     2,    28,  -153,    28,  -153,  -153,  -153,  -153,
      18,    19,    57,    93,   311,   311,    59,   311,  -153,  -153,
      33,    93,   311,   103,    28,    -3,  -153,   113,   122,    63,
    -153,  -153,   311,    28,    28,  -153,  -153,  -153,   102,   111,
      69,   155,  -153,  -153,  -153,   136,     8,  -153,  -153,   311,
     141,  -153,    66,   103,    37,   138,  -153,  -153,   144,  -153,
    -153,   103,    36,   143,   311,  -153,  -153,  -153,    64,   111,
      69,   152,    70,  -153,   142,    66,   170,   168,  -153,   168,
    -153,   311,   311,  -153,  -153,   311,  -153,  -153,  -153,   311,
     311,   311,   311,  -153,  -153,  -153,   259,  -153,   103,  -153,
     173,   174,   175,   169,   171,   272,  -153,  -153,  -153,    28,
     176,  -153,   178,  -153,  -153,  -153,   177,  -153,  -153,  -153,
    -153,  -153,  -153,  -153,  -153,  -153,   197,  -153,   285,   311,
     311,   311,   311,   311,  -153,  -153,  -153,  -153,   311,  -153,
      66,    66,   155,  -153,   198,    66,   199,  -153,    66,    44,
    -153,   311,   311,   212,  -153,  -153,  -153,   200,  -153,  -153,
    -153,  -153,  -153,  -153,  -153,   184,  -153,   184,    97,    97,
      66,  -153,  -153,  -153,   311,   182,   204,   311,  -153,    66,
     246,   246,   201,   194,  -153,   298,   246,   246,   205,   206,
    -153,  -153,   246,   246,  -153,  -153
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       0,     0,     8,     0,     0,     4,     0,     6,     0,     7,
       0,     5,     0,   125,     0,     1,     2,     3,    13,    15,
      16,    20,    18,     0,     9,     0,    12,    10,    21,    22,
       0,     0,     0,     0,     0,     0,     0,     0,    11,    14,
      18,     0,     0,     0,     0,     0,    71,    72,    73,     0,
      19,   112,     0,     0,     0,   118,   119,   120,     0,    36,
      35,    50,    52,    54,    55,    64,    58,    61,    59,     0,
      65,    17,    27,     0,     0,     0,    24,    23,     0,    69,
      76,     0,     0,     0,     0,    29,    33,    32,     0,    34,
      27,   111,    41,    43,    46,    47,     0,    97,    65,    98,
      25,     0,     0,   113,   114,     0,   115,   116,   117,     0,
       0,     0,     0,    99,   100,    53,     0,    67,     0,    26,
       0,     0,     0,     0,     0,     0,    79,    94,    83,     0,
       0,    85,     0,    81,    84,    86,    61,    87,    88,    89,
      90,    91,    92,    68,    70,    77,     0,    28,     0,     0,
       0,     0,     0,     0,   123,   124,   121,   122,     0,    60,
      38,    37,    49,    51,     0,    96,     0,    57,    75,     0,
      66,     0,     0,     0,   109,   110,   104,     0,    93,    80,
      82,    95,    78,    31,    30,    40,    42,    39,    44,    45,
      48,    63,    62,    56,     0,     0,     0,     0,   103,    74,
       0,     0,     0,   101,   105,     0,     0,     0,     0,    61,
     102,   108,     0,     0,   106,   107
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -153,  -153,   107,     0,  -153,  -153,  -153,  -153,   209,  -153,
    -153,   214,  -153,  -153,    32,   -34,   -39,  -153,   -31,   186,
    -153,   -41,    89,   -40,   -28,   135,   -53,  -153,  -153,  -153,
      83,   237,   207,   160,  -153,  -153,  -153,   -38,  -153,  -119,
    -149,  -153,   -67,  -153,  -153,  -153,  -153,  -153,  -153,  -152,
    -153,  -153,  -153,  -153,  -153,     6
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,     4,   128,    44,     7,     8,     9,    10,    18,    19,
      20,    27,    28,    29,    21,    71,    50,    88,   130,    59,
      91,    92,    93,    94,    60,    61,    62,    63,    64,    65,
      66,    11,    45,    46,   169,    47,    48,   131,   132,   133,
     134,   135,    67,   137,   138,   139,   140,   141,   142,    96,
      68,   105,   109,    69,   158,    70
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
       6,    12,    76,    58,     6,    79,    75,    72,    77,    14,
      87,   136,    22,   180,    72,    86,   115,    35,    31,   195,
     196,    90,    81,   111,    95,    15,    36,    33,    82,    31,
      37,    40,    13,    41,    42,   117,   112,     1,     2,     3,
      34,   113,   114,   143,    30,   202,    34,    37,    35,     2,
      80,   203,   204,   146,   197,    30,   163,   210,   211,    98,
      98,    37,   118,   214,   215,   136,    51,    13,    82,   193,
       2,    32,     2,   160,   161,   194,    23,    24,   129,   164,
     170,   166,    43,   165,    73,    25,    26,    52,   168,    49,
      85,   147,   150,   151,   177,   148,    53,    54,    55,    56,
     102,   103,   104,    57,   103,   104,   136,     5,   185,   184,
     187,    16,   188,   189,   183,   154,   155,   156,   157,    49,
      72,    95,    95,    95,    95,    95,     1,     2,     3,    78,
     190,   100,   129,   136,   136,    40,    97,    99,   209,   136,
     136,    83,   101,    95,    95,   136,   136,    51,    13,   120,
      84,   121,   122,   123,   124,   125,     1,     2,   152,   153,
     154,   155,   156,   157,   110,   116,   199,   119,    52,    95,
      78,   126,   145,   129,   208,   149,   127,    53,    54,    55,
      56,    51,    13,   120,    57,   121,   122,   123,   124,   125,
       1,     2,   106,   107,   108,   159,   112,   171,   172,   173,
     206,   174,    52,   175,    78,   179,   150,   200,   178,   181,
     127,    53,    54,    55,    56,    51,    13,   120,    57,   121,
     122,   123,   124,   125,     1,     2,   182,   191,   192,   201,
     212,   213,   198,   205,    39,    89,    52,    38,    78,   186,
     162,    17,   144,    74,   127,    53,    54,    55,    56,    51,
      13,   120,    57,   121,   122,   123,   124,   125,     0,     0,
       0,     0,    51,    13,     0,     0,     0,     0,     0,     0,
      52,     0,    78,     0,     0,    51,    13,     0,   127,    53,
      54,    55,    56,    52,   167,     0,    57,     0,    51,    13,
       0,     0,    53,    54,    55,    56,    52,     0,     0,    57,
       0,    51,    13,     0,   176,    53,    54,    55,    56,    52,
       0,    49,    57,     0,    51,    13,     0,     0,    53,    54,
      55,    56,    52,   207,     0,    57,     0,     0,     0,     0,
       0,    53,    54,    55,    56,    52,     0,     0,    57,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57
};

static const yytype_int16 yycheck[] =
{
       0,     1,    41,    34,     4,    43,    37,    35,    42,     3,
      49,    78,     6,   132,    42,    49,    69,    15,    12,   171,
     172,    49,    25,    15,    52,     0,    24,    15,    31,    23,
      28,    25,     4,    15,    15,    73,    28,    12,    13,    14,
      28,    33,    34,    81,    12,   197,    28,    28,    15,    13,
      44,   200,   201,    84,   173,    23,   109,   206,   207,    53,
      54,    28,    25,   212,   213,   132,     3,     4,    31,    25,
      13,    24,    13,   101,   102,    31,    31,    32,    78,   110,
     118,   112,    25,   111,    25,    31,    32,    24,   116,    26,
      27,    27,    22,    23,   125,    31,    33,    34,    35,    36,
      31,    35,    36,    40,    35,    36,   173,     0,   149,   148,
     151,     4,   152,   153,   148,    18,    19,    20,    21,    26,
     148,   149,   150,   151,   152,   153,    12,    13,    14,    26,
     158,    29,   132,   200,   201,   129,    53,    54,   205,   206,
     207,    28,    31,   171,   172,   212,   213,     3,     4,     5,
      28,     7,     8,     9,    10,    11,    12,    13,    16,    17,
      18,    19,    20,    21,    28,    24,   194,    29,    24,   197,
      26,    27,    29,   173,   205,    23,    32,    33,    34,    35,
      36,     3,     4,     5,    40,     7,     8,     9,    10,    11,
      12,    13,    37,    38,    39,    25,    28,    24,    24,    24,
       6,    32,    24,    32,    26,    27,    22,    25,    32,    32,
      32,    33,    34,    35,    36,     3,     4,     5,    40,     7,
       8,     9,    10,    11,    12,    13,    29,    29,    29,    25,
      25,    25,    32,    32,    25,    49,    24,    23,    26,   150,
     105,     4,    82,    36,    32,    33,    34,    35,    36,     3,
       4,     5,    40,     7,     8,     9,    10,    11,    -1,    -1,
      -1,    -1,     3,     4,    -1,    -1,    -1,    -1,    -1,    -1,
      24,    -1,    26,    -1,    -1,     3,     4,    -1,    32,    33,
      34,    35,    36,    24,    25,    -1,    40,    -1,     3,     4,
      -1,    -1,    33,    34,    35,    36,    24,    -1,    -1,    40,
      -1,     3,     4,    -1,    32,    33,    34,    35,    36,    24,
      -1,    26,    40,    -1,     3,     4,    -1,    -1,    33,    34,
      35,    36,    24,    25,    -1,    40,    -1,    -1,    -1,    -1,
      -1,    33,    34,    35,    36,    24,    -1,    -1,    40,    -1,
      -1,    -1,    -1,    -1,    33,    34,    35,    36,    -1,    -1,
      -1,    40
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    12,    13,    14,    42,    43,    44,    45,    46,    47,
      48,    72,    44,     4,    96,     0,    43,    72,    49,    50,
      51,    55,    96,    31,    32,    31,    32,    52,    53,    54,
      55,    96,    24,    15,    28,    15,    24,    28,    52,    49,
      96,    15,    15,    25,    44,    73,    74,    76,    77,    26,
      57,     3,    24,    33,    34,    35,    36,    40,    59,    60,
      65,    66,    67,    68,    69,    70,    71,    83,    91,    94,
      96,    56,    65,    25,    73,    59,    57,    56,    26,    78,
      96,    25,    31,    28,    28,    27,    56,    57,    58,    60,
      65,    61,    62,    63,    64,    65,    90,    71,    96,    71,
      29,    31,    31,    35,    36,    92,    37,    38,    39,    93,
      28,    15,    28,    33,    34,    67,    24,    78,    25,    29,
       5,     7,     8,     9,    10,    11,    27,    32,    43,    44,
      59,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    78,    74,    29,    59,    27,    31,    23,
      22,    23,    16,    17,    18,    19,    20,    21,    95,    25,
      65,    65,    66,    67,    59,    65,    59,    25,    65,    75,
      78,    24,    24,    24,    32,    32,    32,    59,    32,    27,
      80,    32,    29,    56,    57,    62,    63,    62,    64,    64,
      65,    29,    29,    25,    31,    90,    90,    80,    32,    65,
      25,    25,    90,    81,    81,    32,     6,    25,    59,    83,
      81,    81,    25,    25,    81,    81
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    41,    42,    42,    42,    42,    43,    43,    44,    45,
      46,    46,    47,    48,    48,    49,    49,    50,    50,    51,
      51,    52,    52,    53,    54,    55,    55,    56,    57,    57,
      58,    58,    58,    58,    58,    59,    59,    60,    60,    61,
      61,    61,    62,    62,    63,    63,    63,    64,    64,    65,
      65,    66,    66,    67,    67,    67,    68,    68,    69,    69,
      69,    69,    70,    70,    71,    71,    72,    72,    72,    72,
      73,    73,    74,    74,    75,    75,    76,    77,    77,    78,
      78,    79,    79,    80,    80,    81,    81,    81,    81,    81,
      81,    81,    81,    81,    81,    82,    83,    83,    83,    83,
      83,    84,    84,    85,    85,    86,    87,    87,    87,    88,
      89,    90,    91,    92,    92,    93,    93,    93,    94,    94,
      94,    95,    95,    95,    95,    96
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     2,     1,     1,     1,     1,     1,     2,
       3,     3,     2,     2,     3,     1,     1,     3,     1,     3,
       1,     1,     1,     3,     3,     4,     4,     1,     3,     2,
       3,     3,     1,     1,     1,     1,     1,     3,     3,     3,
       3,     1,     3,     1,     3,     3,     1,     1,     3,     3,
       1,     3,     1,     2,     1,     1,     4,     3,     1,     1,
       3,     1,     4,     4,     1,     1,     6,     5,     6,     5,
       3,     1,     1,     1,     3,     1,     2,     3,     4,     2,
       3,     1,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     2,     1,     2,     3,     2,     2,     2,
       2,     5,     7,     3,     2,     5,     8,     8,     7,     2,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
# ifndef YY_LOCATION_PRINT
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yykind < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yykind], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* CompUnit: CompUnit Decl  */
#line 83 "src/parser.y"
                        { (yyval.root)->body.push_back((yyvsp[0].declare)); }
#line 1362 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 3: /* CompUnit: CompUnit FuncDef  */
#line 84 "src/parser.y"
                           { (yyval.root)->body.push_back((yyvsp[0].fundef)); }
#line 1368 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 4: /* CompUnit: Decl  */
#line 85 "src/parser.y"
               { root = new NRoot(); (yyval.root) = root; (yyval.root)->body.push_back((yyvsp[0].declare)); }
#line 1374 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 5: /* CompUnit: FuncDef  */
#line 86 "src/parser.y"
                  { root = new NRoot(); (yyval.root) = root; (yyval.root)->body.push_back((yyvsp[0].fundef)); }
#line 1380 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 9: /* ConstDeclStmt: ConstDecl SEMI  */
#line 95 "src/parser.y"
                              { (yyval.declare_statement) = (yyvsp[-1].declare_statement); }
#line 1386 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 10: /* ConstDecl: CONST BType ConstDef  */
#line 97 "src/parser.y"
                                { (yyval.declare_statement) = new NDeclareStatement((yyvsp[-1].token)); (yyval.declare_statement)->list.push_back((yyvsp[0].declare)); }
#line 1392 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 11: /* ConstDecl: ConstDecl COMMA ConstDef  */
#line 98 "src/parser.y"
                                    { (yyval.declare_statement)->list.push_back((yyvsp[0].declare)); }
#line 1398 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 12: /* VarDeclStmt: VarDecl SEMI  */
#line 101 "src/parser.y"
                          { (yyval.declare_statement) = (yyvsp[-1].declare_statement); }
#line 1404 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 13: /* VarDecl: BType Def  */
#line 103 "src/parser.y"
                   { (yyval.declare_statement) = new NDeclareStatement((yyvsp[-1].token)); (yyval.declare_statement)->list.push_back((yyvsp[0].declare)); }
#line 1410 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 14: /* VarDecl: VarDecl COMMA Def  */
#line 104 "src/parser.y"
                           { (yyval.declare_statement)->list.push_back((yyvsp[0].declare)); }
#line 1416 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 17: /* DefOne: ident ASSIGN InitVal  */
#line 111 "src/parser.y"
                             { (yyval.declare) = new NVarDeclareWithInit(*(yyvsp[-2].ident), *(yyvsp[0].expr)); }
#line 1422 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 18: /* DefOne: ident  */
#line 112 "src/parser.y"
              { (yyval.declare) = new NVarDeclare(*(yyvsp[0].ident)); }
#line 1428 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 19: /* DefArray: DefArrayName ASSIGN InitValArray  */
#line 115 "src/parser.y"
                                           { (yyval.declare) = new NArrayDeclareWithInit(*(yyvsp[-2].array_identifier), *(yyvsp[0].array_init_value)); }
#line 1434 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 20: /* DefArray: DefArrayName  */
#line 116 "src/parser.y"
                       { (yyval.declare) = new NArrayDeclare(*(yyvsp[0].array_identifier)); }
#line 1440 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 23: /* ConstDefOne: ident ASSIGN InitVal  */
#line 123 "src/parser.y"
                                  { (yyval.declare) = new NVarDeclareWithInit(*(yyvsp[-2].ident), *(yyvsp[0].expr), true); }
#line 1446 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 24: /* ConstDefArray: DefArrayName ASSIGN InitValArray  */
#line 126 "src/parser.y"
                                                { (yyval.declare) = new NArrayDeclareWithInit(*(yyvsp[-2].array_identifier), *(yyvsp[0].array_init_value), true); }
#line 1452 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 25: /* DefArrayName: DefArrayName LSQUARE Exp RSQUARE  */
#line 129 "src/parser.y"
                                               { (yyval.array_identifier) = (yyvsp[-3].array_identifier); (yyval.array_identifier)->shape.push_back((yyvsp[-1].expr)); }
#line 1458 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 26: /* DefArrayName: ident LSQUARE Exp RSQUARE  */
#line 130 "src/parser.y"
                                        { (yyval.array_identifier) = new NArrayIdentifier(*(yyvsp[-3].ident)); (yyval.array_identifier)->shape.push_back((yyvsp[-1].expr)); }
#line 1464 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 28: /* InitValArray: LBRACE InitValArrayInner RBRACE  */
#line 135 "src/parser.y"
                                              { (yyval.array_init_value) = (yyvsp[-1].array_init_value); }
#line 1470 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 29: /* InitValArray: LBRACE RBRACE  */
#line 136 "src/parser.y"
                            { (yyval.array_init_value) = new NArrayDeclareInitValue(false, nullptr); }
#line 1476 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 30: /* InitValArrayInner: InitValArrayInner COMMA InitValArray  */
#line 139 "src/parser.y"
                                                        { (yyval.array_init_value) = (yyvsp[-2].array_init_value); (yyval.array_init_value)->value_list.push_back((yyvsp[0].array_init_value)); }
#line 1482 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 31: /* InitValArrayInner: InitValArrayInner COMMA InitVal  */
#line 140 "src/parser.y"
                                                   { (yyval.array_init_value) = (yyvsp[-2].array_init_value); (yyval.array_init_value)->value_list.push_back(new NArrayDeclareInitValue(true, (yyvsp[0].expr))); }
#line 1488 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 32: /* InitValArrayInner: InitValArray  */
#line 141 "src/parser.y"
                                { (yyval.array_init_value) = new NArrayDeclareInitValue(false, nullptr); (yyval.array_init_value)->value_list.push_back((yyvsp[0].array_init_value)); }
#line 1494 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 33: /* InitValArrayInner: InitVal  */
#line 142 "src/parser.y"
                           { (yyval.array_init_value) = new NArrayDeclareInitValue(false, nullptr); (yyval.array_init_value)->value_list.push_back(new NArrayDeclareInitValue(true, (yyvsp[0].expr))); }
#line 1500 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 34: /* InitValArrayInner: CommaExpr  */
#line 143 "src/parser.y"
                             {
                       (yyval.array_init_value) = new NArrayDeclareInitValue(false, nullptr);
                       for (auto i : dynamic_cast<NCommaExpression*>((yyvsp[0].expr))->values) {
                             (yyval.array_init_value)->value_list.push_back(new NArrayDeclareInitValue(true, i));
                       }
                       delete (yyvsp[0].expr);
                 }
#line 1512 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 37: /* CommaExpr: AddExp COMMA AddExp  */
#line 156 "src/parser.y"
                               {
               auto n = new NCommaExpression();
               n->values.push_back((yyvsp[-2].expr));
               n->values.push_back((yyvsp[0].expr));
               (yyval.expr) = n;
            }
#line 1523 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 38: /* CommaExpr: CommaExpr COMMA AddExp  */
#line 162 "src/parser.y"
                                  {
               dynamic_cast<NCommaExpression*>((yyvsp[-2].expr))->values.push_back((yyvsp[0].expr));
               (yyval.expr) = (yyvsp[-2].expr);
            }
#line 1532 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 39: /* LOrExp: LAndExp OR LAndExp  */
#line 168 "src/parser.y"
                           { (yyval.expr) = new NBinaryExpression(*(yyvsp[-2].expr), (yyvsp[-1].token), *(yyvsp[0].expr)); }
#line 1538 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 40: /* LOrExp: LOrExp OR LAndExp  */
#line 169 "src/parser.y"
                          { (yyval.expr) = new NBinaryExpression(*(yyvsp[-2].expr), (yyvsp[-1].token), *(yyvsp[0].expr)); }
#line 1544 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 42: /* LAndExp: LAndExp AND EqExp  */
#line 173 "src/parser.y"
                            { (yyval.expr) = new NBinaryExpression(*(yyvsp[-2].expr), (yyvsp[-1].token), *(yyvsp[0].expr)); }
#line 1550 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 44: /* EqExp: RelExp EQ RelExp  */
#line 177 "src/parser.y"
                         { (yyval.expr) = new NBinaryExpression(*(yyvsp[-2].expr), (yyvsp[-1].token), *(yyvsp[0].expr)); }
#line 1556 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 45: /* EqExp: RelExp NE RelExp  */
#line 178 "src/parser.y"
                         { (yyval.expr) = new NBinaryExpression(*(yyvsp[-2].expr), (yyvsp[-1].token), *(yyvsp[0].expr)); }
#line 1562 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 48: /* RelExp: RelExp RelOp AddExp  */
#line 183 "src/parser.y"
                             { (yyval.expr) = new NBinaryExpression(*(yyvsp[-2].expr), (yyvsp[-1].token), *(yyvsp[0].expr)); }
#line 1568 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 49: /* AddExp: AddExp AddOp MulExp  */
#line 186 "src/parser.y"
                             { (yyval.expr) = new NBinaryExpression(*(yyvsp[-2].expr), (yyvsp[-1].token), *(yyvsp[0].expr)); }
#line 1574 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 51: /* MulExp: MulExp MulOp UnaryExp  */
#line 190 "src/parser.y"
                               { (yyval.expr) = new NBinaryExpression(*(yyvsp[-2].expr), (yyvsp[-1].token), *(yyvsp[0].expr)); }
#line 1580 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 53: /* UnaryExp: UnaryOp UnaryExp  */
#line 194 "src/parser.y"
                            { (yyval.expr) = new NUnaryExpression((yyvsp[-1].token), *(yyvsp[0].expr)); }
#line 1586 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 56: /* FunctionCall: ident LPAREN FuncRParams RPAREN  */
#line 199 "src/parser.y"
                                              { (yyval.expr) = new NFunctionCall(*(yyvsp[-3].ident), *(yyvsp[-1].arg_list)); }
#line 1592 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 57: /* FunctionCall: ident LPAREN RPAREN  */
#line 200 "src/parser.y"
                                  { (yyval.expr) = new NFunctionCall(*(yyvsp[-2].ident), *(new NFunctionCallArgList())); }
#line 1598 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 60: /* PrimaryExp: LPAREN Cond RPAREN  */
#line 205 "src/parser.y"
                               { (yyval.expr) = (yyvsp[-1].condexp); }
#line 1604 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 62: /* ArrayItem: LVal LSQUARE Exp RSQUARE  */
#line 209 "src/parser.y"
                                    { (yyval.array_identifier) = new NArrayIdentifier(*(yyvsp[-3].ident)); (yyval.array_identifier)->shape.push_back((yyvsp[-1].expr));}
#line 1610 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 63: /* ArrayItem: ArrayItem LSQUARE Exp RSQUARE  */
#line 210 "src/parser.y"
                                         { (yyval.array_identifier) = (yyvsp[-3].array_identifier); (yyval.array_identifier)->shape.push_back((yyvsp[-1].expr));}
#line 1616 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 66: /* FuncDef: BType ident LPAREN FuncFParams RPAREN Block  */
#line 217 "src/parser.y"
                                                     { (yyval.fundef) = new NFunctionDefine((yyvsp[-5].token), *(yyvsp[-4].ident), *(yyvsp[-2].fundefarglist), *(yyvsp[0].block)); }
#line 1622 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 67: /* FuncDef: BType ident LPAREN RPAREN Block  */
#line 218 "src/parser.y"
                                         { (yyval.fundef) = new NFunctionDefine((yyvsp[-4].token), *(yyvsp[-3].ident), *(new NFunctionDefineArgList()), *(yyvsp[0].block)); }
#line 1628 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 68: /* FuncDef: VOID ident LPAREN FuncFParams RPAREN Block  */
#line 219 "src/parser.y"
                                                    { (yyval.fundef) = new NFunctionDefine((yyvsp[-5].token), *(yyvsp[-4].ident), *(yyvsp[-2].fundefarglist), *(yyvsp[0].block)); }
#line 1634 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 69: /* FuncDef: VOID ident LPAREN RPAREN Block  */
#line 220 "src/parser.y"
                                        { (yyval.fundef) = new NFunctionDefine((yyvsp[-4].token), *(yyvsp[-3].ident), *(new NFunctionDefineArgList()), *(yyvsp[0].block)); }
#line 1640 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 70: /* FuncFParams: FuncFParams COMMA FuncFParam  */
#line 224 "src/parser.y"
                                          { (yyval.fundefarglist)->list.push_back((yyvsp[0].fundefarg)); }
#line 1646 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 71: /* FuncFParams: FuncFParam  */
#line 225 "src/parser.y"
                        {{ (yyval.fundefarglist) = new NFunctionDefineArgList(); (yyval.fundefarglist)->list.push_back((yyvsp[0].fundefarg)); }}
#line 1652 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 74: /* FuncRParams: FuncRParams COMMA AddExp  */
#line 232 "src/parser.y"
                                      { (yyval.arg_list) = (yyvsp[-2].arg_list); (yyval.arg_list)->args.push_back((yyvsp[0].expr)); }
#line 1658 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 75: /* FuncRParams: AddExp  */
#line 233 "src/parser.y"
                    { (yyval.arg_list) = new NFunctionCallArgList(); (yyval.arg_list)->args.push_back((yyvsp[0].expr)); }
#line 1664 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 76: /* FuncFParamOne: BType ident  */
#line 236 "src/parser.y"
                           { (yyval.fundefarg) = new NFunctionDefineArg((yyvsp[-1].token), *(yyvsp[0].ident)); }
#line 1670 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 77: /* FuncFParamArray: FuncFParamOne LSQUARE RSQUARE  */
#line 238 "src/parser.y"
                                               {
                        (yyval.fundefarg) = new NFunctionDefineArg(
                              (yyvsp[-2].fundefarg)->type,
                              *new NArrayIdentifier(*(new NArrayIdentifier((yyvsp[-2].fundefarg)->name))));
                        ((NArrayIdentifier*)&((yyval.fundefarg)->name))->shape.push_back(new NNumber(1));
                        delete (yyvsp[-2].fundefarg);
                  }
#line 1682 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 78: /* FuncFParamArray: FuncFParamArray LSQUARE Exp RSQUARE  */
#line 245 "src/parser.y"
                                                     { (yyval.fundefarg) = (yyvsp[-3].fundefarg); ((NArrayIdentifier*)&((yyval.fundefarg)->name))->shape.push_back((yyvsp[-1].expr));; }
#line 1688 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 79: /* Block: LBRACE RBRACE  */
#line 248 "src/parser.y"
                     { (yyval.block) = new NBlock(); }
#line 1694 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 80: /* Block: LBRACE BlockItems RBRACE  */
#line 249 "src/parser.y"
                                { (yyval.block) = (yyvsp[-1].block); }
#line 1700 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 81: /* BlockItems: BlockItem  */
#line 252 "src/parser.y"
                      { (yyval.block) = new NBlock(); (yyval.block)->statements.push_back((yyvsp[0].stmt)); }
#line 1706 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 82: /* BlockItems: BlockItems BlockItem  */
#line 253 "src/parser.y"
                                 { (yyval.block) = (yyvsp[-1].block); (yyval.block)->statements.push_back((yyvsp[0].stmt)); }
#line 1712 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 93: /* Stmt: Exp SEMI  */
#line 268 "src/parser.y"
               { (yyval.stmt) = new  NEvalStatement(*(yyvsp[-1].expr)); }
#line 1718 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 94: /* Stmt: SEMI  */
#line 269 "src/parser.y"
           { (yyval.stmt) = new NVoidStatement(); }
#line 1724 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 95: /* AssignStmt: AssignStmtWithoutSemi SEMI  */
#line 272 "src/parser.y"
                                       { (yyval.stmt) = (yyvsp[-1].stmt); }
#line 1730 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 96: /* AssignStmtWithoutSemi: LVal ASSIGN AddExp  */
#line 274 "src/parser.y"
                                          { (yyval.stmt) = new NAssignment(*(yyvsp[-2].ident), *(yyvsp[0].expr)); }
#line 1736 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 97: /* AssignStmtWithoutSemi: PLUSPLUS LVal  */
#line 275 "src/parser.y"
                                     { (yyval.stmt) = new NAssignment(*(yyvsp[0].ident), *new NBinaryExpression(*(yyvsp[0].ident), PLUS, *new NNumber(1))); }
#line 1742 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 98: /* AssignStmtWithoutSemi: MINUSMINUS LVal  */
#line 276 "src/parser.y"
                                       { (yyval.stmt) = new NAssignment(*(yyvsp[0].ident), *new NBinaryExpression(*(yyvsp[0].ident), MINUS, *new NNumber(1))); }
#line 1748 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 99: /* AssignStmtWithoutSemi: LVal PLUSPLUS  */
#line 277 "src/parser.y"
                                     { (yyval.stmt) = new NAfterInc(*(yyvsp[-1].ident), PLUS); }
#line 1754 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 100: /* AssignStmtWithoutSemi: LVal MINUSMINUS  */
#line 278 "src/parser.y"
                                       { (yyval.stmt) = new NAfterInc(*(yyvsp[-1].ident), MINUS); }
#line 1760 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 101: /* IfStmt: IF LPAREN Cond RPAREN Stmt  */
#line 281 "src/parser.y"
                                   { (yyval.stmt) = new NIfStatement(*(yyvsp[-2].condexp), *(yyvsp[0].stmt)); }
#line 1766 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 102: /* IfStmt: IF LPAREN Cond RPAREN Stmt ELSE Stmt  */
#line 282 "src/parser.y"
                                             { (yyval.stmt) = new NIfElseStatement(*(yyvsp[-4].condexp), *(yyvsp[-2].stmt), *(yyvsp[0].stmt)); }
#line 1772 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 103: /* ReturnStmt: RETURN Exp SEMI  */
#line 285 "src/parser.y"
                            { (yyval.stmt) = new NReturnStatement((yyvsp[-1].expr)); }
#line 1778 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 104: /* ReturnStmt: RETURN SEMI  */
#line 286 "src/parser.y"
                        { (yyval.stmt) = new NReturnStatement(); }
#line 1784 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 105: /* WhileStmt: WHILE LPAREN Cond RPAREN Stmt  */
#line 289 "src/parser.y"
                                         { (yyval.stmt) = new NWhileStatement(*(yyvsp[-2].condexp), *(yyvsp[0].stmt));}
#line 1790 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 106: /* ForStmt: FOR LPAREN BlockItem Cond SEMI Exp RPAREN Stmt  */
#line 291 "src/parser.y"
                                                        {
                  auto b = new NBlock();
                  auto do_block = new NBlock();
                  do_block->statements.push_back((yyvsp[0].stmt));
                  do_block->statements.push_back(new NEvalStatement(*(yyvsp[-2].expr)));
                  b->statements.push_back((yyvsp[-5].stmt));
                  b->statements.push_back(new NWhileStatement(*(yyvsp[-4].condexp), *do_block));
                  (yyval.stmt) = b;
            }
#line 1804 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 107: /* ForStmt: FOR LPAREN BlockItem Cond SEMI AssignStmtWithoutSemi RPAREN Stmt  */
#line 300 "src/parser.y"
                                                                          {
                  auto b = new NBlock();
                  auto do_block = new NBlock();
                  do_block->statements.push_back((yyvsp[0].stmt));
                  do_block->statements.push_back((yyvsp[-2].stmt));
                  b->statements.push_back((yyvsp[-5].stmt));
                  b->statements.push_back(new NWhileStatement(*(yyvsp[-4].condexp), *do_block));
                  (yyval.stmt) = b;
            }
#line 1818 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 108: /* ForStmt: FOR LPAREN BlockItem Cond SEMI RPAREN Stmt  */
#line 309 "src/parser.y"
                                                    {
                  auto b = new NBlock();
                  auto do_block = (yyvsp[0].stmt);
                  b->statements.push_back((yyvsp[-4].stmt));
                  b->statements.push_back(new NWhileStatement(*(yyvsp[-3].condexp), *do_block));
                  (yyval.stmt) = b;
            }
#line 1830 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 109: /* BreakStmt: BREAK SEMI  */
#line 318 "src/parser.y"
                      { (yyval.stmt) = new NBreakStatement(); }
#line 1836 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 110: /* ContinueStmt: CONTINUE SEMI  */
#line 320 "src/parser.y"
                            { (yyval.stmt) = new NContinueStatement(); }
#line 1842 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 112: /* Number: INTEGER_VALUE  */
#line 324 "src/parser.y"
                      { (yyval.expr) = new NNumber(*(yyvsp[0].string)); }
#line 1848 "/home/zippermonkey/syc/build/parser.cpp"
    break;

  case 125: /* ident: IDENTIFIER  */
#line 346 "src/parser.y"
                  { (yyval.ident) = new NIdentifier(*(yyvsp[0].string)); }
#line 1854 "/home/zippermonkey/syc/build/parser.cpp"
    break;


#line 1858 "/home/zippermonkey/syc/build/parser.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturn;
#endif


/*-------------------------------------------------------.
| yyreturn -- parsing is finished, clean up and return.  |
`-------------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

